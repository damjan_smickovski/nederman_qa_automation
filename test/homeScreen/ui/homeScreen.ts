import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element } from 'protractor';
import { TargetLocator } from 'selenium-webdriver';

// TODO Order elements by sections
export const homeScreen = {
    nedermanHoldingOption:    Target.the('Tenant dropdown menu').located(by.cssContainingText('option', 'Nederman Holding AB (PRODUCTION)')),
    alarmButton:  Target.the('Alarm button').located(by.cssContainingText('.nav-parent', 'Alarm center')),
    actionCenterButton:  Target.the('Action  button').located(by.cssContainingText('.nav-parent', 'Action center')),
    dashboardsButton:  Target.the('Dashboards  button').located(by.cssContainingText('.nav-parent', 'Dashboards')),
    userPreferencesButton:  Target.the('User Preferences button').located(by.id('initialStep8')),
    languageButton:  Target.the('Language  button').located(by.xpath('//a[contains(text(), "Language")]')),
    setAsEntryPointButton:  Target.the('Set as entry point button').located(by.xpath('//a[contains(text(), "Set as")]')),
    dotPreferencesButton:  Target.the('Dot preferences button').located(by.xpath('//a[contains(text(), "Dot preference")]')),
    YouSignedOutOfYourAccountText: Target.the('You Signed Out Of Your Account text home page').located(by.id('login_workload_logo_text')),
    administrationButton:  Target.the('Administration button').located(by.id('initialStep9')),
    locationButton: Target.the('Location button').located(by.xpath('//a[contains(text(), "Locations")]')),
    systemsButton: Target.the('Systems button').located(by.xpath('//a[contains(text(), "Systems")]')),
    editButton: Target.the('Edit button').located(by.cssContainingText('.text-center ', 'Edit')),
    thingsButton: Target.the('Things button').located(by.xpath('//a[contains(text(), "Things")]')),
    ruleEngineButton: Target.the('Rule engine button').located(by.xpath('//a[contains(text(), "Rule engine")]')),
    addNewButton:  Target.the('Add New button').located(by.id('addNewDashboard')),
    logOutButton: Target.the('Log out button').located(by.xpath('//a[contains(text(), "Log out")]')),
   

};
