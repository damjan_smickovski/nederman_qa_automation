import { expect } from 'chai';
import { browser, by, element, promise, protractor } from 'protractor';
import { reject } from 'q';
import { Actor, Click, Enter, Is, Scroll, See, Task, Text, Wait } from 'serenity-js/lib/screenplay-protractor';
import { BrowseTheWeb, Duration, Select } from 'serenity-js/lib/serenity-protractor';
import { equals } from '../assertions';
import { homeScreen } from './ui/homeScreen';
const waitTime = Duration.ofMillis(30000);
const damjan = Actor.named('damjan').whoCan(BrowseTheWeb.using(protractor.browser));


// TODO Remove of and export a single function with methods
export const clickAlarmCenter = ({
    of: () => Task.where(`#actor clicks Alarm`,

        Wait.upTo(waitTime).until(homeScreen.alarmButton, Is.visible()),
        Scroll.to(homeScreen.alarmButton),
        Click.on(homeScreen.alarmButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickActionCenter = ({
    of: () => Task.where(`#actor clicks Action Center`,

        Wait.upTo(waitTime).until(homeScreen.actionCenterButton, Is.visible()),
        Scroll.to(homeScreen.actionCenterButton),
        Click.on(homeScreen.actionCenterButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickDashboards = ({
    of: () => Task.where(`#actor clicks Dashboards`,

        Wait.upTo(waitTime).until(homeScreen.dashboardsButton, Is.visible()),
        Scroll.to(homeScreen.dashboardsButton),
        Click.on(homeScreen.dashboardsButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickUserPreferences = ({
    of: () => Task.where(`#actor clicks User Preferences`,

        Wait.upTo(waitTime).until(homeScreen.userPreferencesButton, Is.visible()),
        Scroll.to(homeScreen.userPreferencesButton),
        Click.on(homeScreen.userPreferencesButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickLanguage = ({
    of: () => Task.where(`#actor clicks Language`,

        Wait.upTo(waitTime).until(homeScreen.languageButton, Is.visible()),
        Scroll.to(homeScreen.languageButton),
        Click.on(homeScreen.languageButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickSetAsEntryPoint = ({
    of: () => Task.where(`#actor clicks Set as entry point`,

        Wait.upTo(waitTime).until(homeScreen.setAsEntryPointButton, Is.visible()),
        Scroll.to(homeScreen.setAsEntryPointButton),
        Click.on(homeScreen.setAsEntryPointButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickDotPreferences = ({
    of: () => Task.where(`#actor clicks Dot preferences`,

        Wait.upTo(waitTime).until(homeScreen.dotPreferencesButton, Is.visible()),
        Scroll.to(homeScreen.dotPreferencesButton),
        Click.on(homeScreen.dotPreferencesButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickLogOut = ({
    of: () => Task.where(`#actor clicks Log out`,

        Wait.upTo(waitTime).until(homeScreen.logOutButton, Is.visible()),
        Scroll.to(homeScreen.logOutButton),
        Click.on(homeScreen.logOutButton),

    ),
});
// TODO Remove of and export a single function with methods
export const checkIfYouSignedOutOfYourAccountTextIsVisible = ({
    of: () => Task.where(`#actor clicks sign out`,
        Wait.upTo(waitTime).until(homeScreen.YouSignedOutOfYourAccountText, Is.visible()),
    ),

    checkIfYouSignedOutOfYourAccountTextIsVisible() {
        return new Promise<void>((resolve, _reject) => {
            Wait.upTo(waitTime).until(homeScreen.YouSignedOutOfYourAccountText, Is.visible());
            resolve();
        });
    },
});
// TODO Remove of and export a single function with methods
export const clickAdministration = ({
    of: () => Task.where(`#actor clicks Administration`,

        Wait.upTo(waitTime).until(homeScreen.administrationButton, Is.visible()),
        Scroll.to(homeScreen.administrationButton),
        Click.on(homeScreen.administrationButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickLocation = ({
    of: () => Task.where(`#actor clicks Location`,

        Wait.upTo(waitTime).until(homeScreen.locationButton, Is.visible()),
        Scroll.to(homeScreen.locationButton),
        Click.on(homeScreen.locationButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickSystems = ({
    of: () => Task.where(`#actor clicks Systems`,

        Wait.upTo(waitTime).until(homeScreen.systemsButton, Is.visible()),
        Scroll.to(homeScreen.systemsButton),
        Click.on(homeScreen.systemsButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickEdit = ({
    of: () => Task.where(`#actor clicks Edit`,

        Wait.upTo(waitTime).until(homeScreen.editButton, Is.visible()),
        Scroll.to(homeScreen.editButton),
        Click.on(homeScreen.editButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickThings = ({
    of: () => Task.where(`#actor clicks Things`,

        Wait.upTo(waitTime).until(homeScreen.thingsButton, Is.visible()),
        Scroll.to(homeScreen.thingsButton),
        Click.on(homeScreen.thingsButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickRuleEngine = ({
    of: () => Task.where(`#actor clicks Rule engine`,

        Wait.upTo(waitTime).until(homeScreen.ruleEngineButton, Is.visible()),
        Scroll.to(homeScreen.ruleEngineButton),
        Click.on(homeScreen.ruleEngineButton),

    ),
});
// TODO Remove of and export a single function with methods
export const clickAddNew = ({
    of: () => Task.where(`#actor clicks Add New`,

        Wait.upTo(waitTime).until(homeScreen.addNewButton, Is.visible()),
        Scroll.to(homeScreen.addNewButton),
        Click.on(homeScreen.addNewButton),

    ),
});
// TODO Remove of and export a single function with methods
export const selectNedermanHoldingFromTenantDropdown = ({
    of: () => Task.where(`#actor clicks Dashboards`,

        Wait.upTo(waitTime).until(homeScreen.nedermanHoldingOption, Is.visible()),
        Scroll.to(homeScreen.nedermanHoldingOption),
        Click.on(homeScreen.nedermanHoldingOption),
    ),
});

