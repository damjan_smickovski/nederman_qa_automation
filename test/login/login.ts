import { Enter, Is, Scroll, Task, Wait, Click } from 'serenity-js/lib/screenplay-protractor';
import { loginScreen } from './ui/loginScreen';
import { Duration } from 'serenity-js/lib/serenity-protractor';
import { expect } from 'chai';
import { browser, promise, element, by } from 'protractor';


const waitTime = Duration.ofMillis(30000);
// TODO Remove of and export a single function with methods
export const enterEmail = ({
    of: (email: string) => Task.where(`#actor enters their email as ${email}`,
        Wait.upTo(waitTime).until(loginScreen.emailField, Is.visible(), ),
        Scroll.to(loginScreen.emailField),
        Enter.theValue(email).into(loginScreen.emailField),
    ),

    setLogIn: () => {
        this.loggedIn = true;
    }
});

// TODO Remove of and export a single function with methods
export const clickNext = ({
    of: () => Task.where(`#actor clicks next`,
        Wait.upTo(waitTime).until(loginScreen.nextButton, Is.visible()),
        Scroll.to(loginScreen.nextButton),
        Click.on(loginScreen.nextButton)
    ),
});



// TODO Remove of and export a single function with methods
export const enterPassword = ({
    of: (password: string) => Task.where(`#actor enters their password as ${password}`,
        Wait.upTo(waitTime).until(loginScreen.passwordField, Is.visible()),
        Scroll.to(loginScreen.passwordField),
        Enter.theValue(password).into(loginScreen.passwordField),
    ),
});


// TODO Remove of and export a single function with methods
export const clickSignIn = ({
    of: () => Task.where(`#actor clicks sign in`,
        Wait.upTo(waitTime).until(loginScreen.signInButton, Is.visible()),
        Scroll.to(loginScreen.signInButton),
        Click.on(loginScreen.signInButton)
    ),
});

// TODO Remove of and export a single function with methods
export const reduceLoginNo = ({
    of: () => Task.where(`#actor clicks sign in`,
        Wait.upTo(waitTime).until(loginScreen.reduceLoginNoButton, Is.visible()),
        Scroll.to(loginScreen.reduceLoginNoButton),
        Click.on(loginScreen.reduceLoginNoButton)
    ),
});

// TODO Remove of and export a single function with methods
export const checkIfInsightLogoIsVisible = ({
    of: () => Task.where(`#actor clicks sign in`,
        Wait.upTo(waitTime).until(loginScreen.insightLogo, Is.visible())
    ),
// TODO Remove promise and return Task object
    checkIfLogoIsVisible: function () {
        return new Promise<void>((resolve, reject) => {
            Wait.upTo(waitTime).until(loginScreen.insightLogo, Is.visible())
            resolve();
        })
    },

});
// TODO Remove of and export a single function with methods
export const clickOk = ({
    of: () => Task.where(`#actor clicks OK`,
        Wait.upTo(waitTime).until(loginScreen.okButton, Is.visible()),
        Scroll.to(loginScreen.okButton),
        Click.on(loginScreen.okButton),
    ),
});
