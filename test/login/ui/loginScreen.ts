import { Target } from 'serenity-js/lib/screenplay-protractor';

import { by, element } from 'protractor';


// TODO Group elements by sections
export const loginScreen = {
    emailField: Target.the('Email field').located(by.id('i0116')),
    nextButton: Target.the('Next button').located(by.id('idSIButton9')),
    passwordField:    Target.the('Password field').located(by.id('i0118')),
    signInButton:    Target.the('Sign in button').located(by.id('idSIButton9')),
    reduceLoginNoButton:    Target.the('Reduce login times no button').located(by.id('idBtn_Back')),
    insightLogo:    Target.the('Insight logo home page').located(by.xpath('//img[@alt="logo"]')),
    okButton: Target.the('OK button').located(by.cssContainingText('.btn', 'OK')),
};
