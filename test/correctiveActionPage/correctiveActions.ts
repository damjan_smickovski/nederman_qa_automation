import { expect } from 'chai';
import { browser, by, element, promise, protractor, ProtractorBrowser } from 'protractor';
import { reject } from 'q';
import { Actor, Click, Enter, Is, Scroll, See, Task, Text, Wait } from 'serenity-js/lib/screenplay-protractor';
import { BrowseTheWeb, Duration, Select, Target, Execute } from 'serenity-js/lib/serenity-protractor';
import { equals } from '../assertions';
import { correctiveActionElements } from './ui/correctiveActionElements';
const waitTime = Duration.ofMillis(30000);
const damjan = Actor.named('damjan').whoCan(BrowseTheWeb.using(protractor.browser));
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';


export const correctiveActions = ({


    enterCAName: (CAname: string) => Task.where(`#actor enters the name of the CA`,
        Wait.upTo(waitTime).until(correctiveActionElements.caName, Is.visible()),
        Scroll.to(correctiveActionElements.caName),
        Enter.theValue(CAname).into(correctiveActionElements.caName),
    ),


    selectCAType: (CAType: string) => {
        let elem: Target;

        switch (CAType) {
            case 'Compliance':
                elem = correctiveActionElements.complianceOption;
                break;

            case 'Maintenance':
                elem = correctiveActionElements.maintenanceOption;
                break;

            case 'Other':
                elem = correctiveActionElements.otherOption;
                break;
        }

        return Task.where(`#actor selects the ${CAType} type option`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    selectStartDate: () => Task.where(`#actor clicks the start date button and selects a date`,
        Wait.upTo(waitTime).until(correctiveActionElements.startDateButton, Is.clickable()),
        Scroll.to(correctiveActionElements.startDateButton),
        Click.on(correctiveActionElements.startDateButton),
        Wait.upTo(waitTime).until(correctiveActionElements.startDate, Is.clickable()),
        Click.on(correctiveActionElements.startDate),
    ),

    selectEndDate: () => Task.where(`#actor clicks the end date button and selects a date`,
        Wait.upTo(waitTime).until(correctiveActionElements.endDateButon, Is.clickable()),
        Scroll.to(correctiveActionElements.endDateButon),
        Click.on(correctiveActionElements.endDateButon),
        Wait.upTo(waitTime).until(correctiveActionElements.endDate, Is.clickable()),
        Click.on(correctiveActionElements.endDate),
    ),

    enterCACause: (CACause: string) => Task.where(`#actor enters the name of the CA`,
        Wait.upTo(waitTime).until(correctiveActionElements.caCause, Is.visible()),
        Scroll.to(correctiveActionElements.caCause),
        Enter.theValue(CACause).into(correctiveActionElements.caCause),
    ),

    enterCAResponse: (CAResponse: string) => Task.where(`#actor enters the name of the CA`,
        Wait.upTo(waitTime).until(correctiveActionElements.caResponse, Is.visible()),
        Scroll.to(correctiveActionElements.caResponse),
        Enter.theValue(CAResponse).into(correctiveActionElements.caResponse),
    ),


    clickAddButton: () => Task.where(`#actor clicks the add button and submits CA`,
        Wait.upTo(waitTime).until(correctiveActionElements.addButton, Is.clickable()),
        Scroll.to(correctiveActionElements.addButton),
        Click.on(correctiveActionElements.addButton),
    ),


});
