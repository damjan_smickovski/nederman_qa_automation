import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element } from 'protractor';
import { TargetLocator } from 'selenium-webdriver';



let date = new Date();
var day = date.getDay();


export const correctiveActionElements = {



    caName: Target.the('Corrective action name field').located(by.id(`caname`)),

    // Type options elements
    complianceOption: Target.the('Corrective action compliance option').located(by.xpath(`//*[@id="catype"]/option[text()="Compliance"]`)),
    maintenanceOption: Target.the('Corrective action maintenance option').located(by.xpath(`//*[@id="catype"]/option[text()="Maintenance"]`)),
    otherOption: Target.the('Corrective action other option').located(by.xpath(`//*[@id="catype"]/option[text()="Maintenance"]`)),

    // Date elements
    startDateButton: Target.the('Sart date button').located(by.id(`start`)),
    endDateButon: Target.the('End date button').located(by.id(`end`)),
    startDate: Target.the('Starting date').located(by.xpath(`//table[@class="days weeks"]//span[text()="${day}"]`)),
    endDate: Target.the('Ending date').located(by.xpath(`//table[@class="days weeks"]//span[text()="${day + 1}"]`)),

    // Description fields
    caCause: Target.the('Corrective action cause field').located(by.id(`cause`)),
    caResponse: Target.the('Corrective action response').located(by.id(`response`)),

    // Buttons
    addButton: Target.the('Add button').located(by.xpath(`//button[contains(text(),"Add")]`)),
    cancelButton: Target.the('Cancel button').located(by.id(`backCancel`)),

    

};
