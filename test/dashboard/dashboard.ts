import { Enter, Is, Scroll, Task, Wait, Click, BrowseTheWeb, Actor, DoubleClick, See } from 'serenity-js/lib/screenplay-protractor';
import { dashboard } from './ui/dashboardElements';
import { Duration } from 'serenity-js/lib/serenity-protractor';
import chai = require('chai');
chai.use(require('chai-smoothie'));
const expect = chai.expect;

import { browser, promise, element, by, protractor, Browser } from 'protractor';
import { reject } from 'q';
const waitTime = Duration.ofMillis(30000);
let damjan = Actor.named('James').whoCan(BrowseTheWeb.using(protractor.browser))
// TODO Remove of and export a single function with methods
export const selectDamjanTestDashboard = ({
    of: () => Task.where(`#actor selects InredoAB`,
        Wait.upTo(waitTime).until(dashboard.damjanTestDashboard, Is.visible()),
        Scroll.to(dashboard.damjanTestDashboard),
        Click.on(dashboard.damjanTestDashboard),
    ),
});

// TODO Remove of and export a single function with methods
export const clickMoreOptionsButton = ({
    of: () => Task.where(`#actor selects InredoAB`,
        Wait.upTo(waitTime).until(dashboard.dashboardMoreOptionsButton, Is.visible()),
        Scroll.to(dashboard.dashboardMoreOptionsButton),
        Click.on(dashboard.dashboardMoreOptionsButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickEditButton = ({
    of: () => Task.where(`#actor clicks edit button`,
        Wait.upTo(waitTime).until(dashboard.dashboardEditButton, Is.visible()),
        Scroll.to(dashboard.dashboardEditButton),
        Click.on(dashboard.dashboardEditButton),
    ),

});

// TODO Remove of and export a single function with methods
export const clickCreateLayout = ({
    of: () => Task.where(`#actor clicks create layout`,
        Wait.upTo(waitTime).until(dashboard.createLayoutButton, Is.visible()),
        Scroll.to(dashboard.createLayoutButton),
        Click.on(dashboard.createLayoutButton),
    ),
});

// TODO Edit to return Task object
export const deleteWidget = async () => {
    return new Promise(async (resolve, reject) => {
        await Wait.upTo(waitTime).until(dashboard.removeWidgetButton, Is.present());
        await BrowseTheWeb.as(damjan).locateAll(dashboard.draggedLayoutArea).browser_.driver.actions().mouseMove(BrowseTheWeb.as(damjan).locate(dashboard.draggedLayoutArea)).perform();
        await BrowseTheWeb.as(damjan).locateAll(dashboard.removeWidgetButton).get(1).click();
        resolve();
    })
}

// TODO Edit to return Task object
export const dragAndDropLayout = async () => {
    return new Promise(async (resolve, reject) => {
        Wait.upTo(waitTime).until(dashboard.oneColumnLayout, Is.visible())
        await BrowseTheWeb.as(damjan).locateAll(dashboard.oneColumnLayout).browser_.driver.actions()
            .mouseDown(BrowseTheWeb.as(damjan).locate(dashboard.oneColumnLayout))
            .mouseMove(BrowseTheWeb.as(damjan).locate(dashboard.draggedLayoutArea))
            .mouseUp(BrowseTheWeb.as(damjan).locate(dashboard.draggedLayoutArea))
            .perform()
        resolve();
    }).catch(err => { reject(err) })
}

// TODO Edit to return Task object
export const rightClickSuperWidget = async () => {
    return new Promise(async (resolve, reject) => {
        Wait.upTo(waitTime).until(dashboard.draggedSupperWidget, Is.visible())
        await BrowseTheWeb.as(damjan).locateAll(dashboard.draggedSupperWidget).browser_.driver.actions().mouseMove(BrowseTheWeb.as(damjan).locate(dashboard.draggedSupperWidget)).perform();
        browser.actions().click(protractor.Button.RIGHT).perform();
        resolve();
    }).catch(err => { reject(err) })
}
// TODO Remove of and export a single function with methods
export const editSuperWidget = ({
    of: () => Task.where(`#actor clicks create layout`,
        Wait.upTo(waitTime).until(dashboard.editWidgetButton, Is.visible()),
        Scroll.to(dashboard.editWidgetButton),
        Click.on(dashboard.editWidgetButton),
    ),
});

// TODO Remove of and export a single function with methods
export const selectThe160pxOption = ({
    of: () => Task.where(`#actor clicks create layout`,
        Wait.upTo(waitTime).until(dashboard.size160pxLayoutRadioButton, Is.visible()),
        Scroll.to(dashboard.size160pxLayoutRadioButton),
        Click.on(dashboard.size160pxLayoutRadioButton),
    ),
});
// TODO Remove of and export a single function with methods
export const clickComponentsButton = ({
    of: () => Task.where(`#actor clicks panels button`,
        Wait.upTo(waitTime).until(dashboard.componentsButton, Is.visible()),
        Scroll.to(dashboard.componentsButton),
        Click.on(dashboard.componentsButton),
    ),
});

export const fillOutSensorDetails = ({
    enterLabel: () => Task.where(`#actor enters label of sensor`,
        Wait.upTo(waitTime).until(dashboard.labelField, Is.visible()),
        Scroll.to(dashboard.labelField),
        Enter.theValue('Automated sensor').into(dashboard.labelField)
    ),

    enterDescription: () => Task.where(`#actor enters description of sensor`,
        Wait.upTo(waitTime).until(dashboard.labelField, Is.visible()),
        Scroll.to(dashboard.labelField),
        Enter.theValue('Some description').into(dashboard.descriptionField)
    ),

    selectTheButtonSensor: () => Task.where(`#actor selects the button sensor`,
        Wait.upTo(waitTime).until(dashboard.selectButtonSensor, Is.clickable()),
        Scroll.to(dashboard.selectButtonSensor),
        Click.on(dashboard.selectButtonSensor)
    ),

    clickOverrideSensorProperties: () => Task.where(`#actor clicks the ovveride sensor properties radio button`,
        Wait.upTo(waitTime).until(dashboard.overrideSensorCheckbox, Is.clickable()),
        Scroll.to(dashboard.overrideSensorCheckbox),
        Click.on(dashboard.overrideSensorCheckbox)
    ),

    enterMinValue: () => Task.where(`#actor enters the min value`,
        Wait.upTo(waitTime).until(dashboard.minValueField, Is.visible()),
        Scroll.to(dashboard.minValueField),
        Enter.theValue(0).into(dashboard.minValueField)
    ),

    enterMaxValue: () => Task.where(`#actor enters the max value`,
        Wait.upTo(waitTime).until(dashboard.maxValueField, Is.visible()),
        Scroll.to(dashboard.maxValueField),
        Enter.theValue(1).into(dashboard.maxValueField)
    ),

    selectUnitFromDropdown: () => Task.where(`#actor selects the Hz unit`,
        Wait.upTo(waitTime).until(dashboard.selectHzUnit, Is.clickable()),
        Scroll.to(dashboard.selectHzUnit),
        Click.on(dashboard.selectHzUnit)
    ),

    clickAddRangeButton: () => Task.where(`#actor clicks the add range button`,
        Wait.upTo(waitTime).until(dashboard.addRangeButton, Is.clickable()),
        Scroll.to(dashboard.addRangeButton),
        Click.on(dashboard.addRangeButton)
    ),

    enterRange1Label: () => Task.where(`#actor enters the label for range 1`,
        Wait.upTo(waitTime).until(dashboard.range1LabelField, Is.visible()),
        Scroll.to(dashboard.range1LabelField),
        Enter.theValue('off').into(dashboard.range1LabelField)
    ),

    enterToRange1: () => Task.where(`#actor enters the TO field for range 1`,
        Wait.upTo(waitTime).until(dashboard.toField1, Is.visible()),
        Scroll.to(dashboard.toField1),
        Enter.theValue(0).into(dashboard.toField1)
    ),


    enterRange2Label: () => Task.where(`#actor enters the label for range 2`,
        Wait.upTo(waitTime).until(dashboard.range2LabelField, Is.visible()),
        Scroll.to(dashboard.range2LabelField),
        Enter.theValue('on').into(dashboard.range2LabelField)
    ),

    enterFromRange2: () => Task.where(`#actor enters the FROM field for range 2`,
        Wait.upTo(waitTime).until(dashboard.fromField2, Is.visible()),
        Scroll.to(dashboard.fromField2),
        Enter.theValue(1).into(dashboard.fromField2)
    ),

    clickAddToDashboardButton: () => Task.where(`#actor clicks the add to dashboard button`,
        Wait.upTo(waitTime).until(dashboard.addToDashboardButton, Is.clickable()),
        Scroll.to(dashboard.addToDashboardButton),
        Click.on(dashboard.addToDashboardButton)
    ),

});

// TODO Remove of and export a single function with methods
export const clickLayoutsButton = ({
    of: () => Task.where(`#actor clicks text & images button`,
        Wait.upTo(waitTime).until(dashboard.layoutsButton, Is.visible()),
        Scroll.to(dashboard.layoutsButton),
        Click.on(dashboard.layoutsButton),
    ),
});
// TODO Edit to return Task object
export const dragAndDropWidget = async (widgetToDrag) => {
    return new Promise(async (resolve, reject) => {
        let elem;
        let draggedElem;
        switch (widgetToDrag) {

            case 'super widget': {
                elem = dashboard.superWidgetImageBox;
                draggedElem = dashboard.emptyLayoutArea;
                break;
            }

            case 'super chart': {
                elem = dashboard.superChartImageBox;
                draggedElem = dashboard.emptyLayoutArea;
                break;
            }

            case 'super table': {
                elem = dashboard.superTableImageBox;
                draggedElem = dashboard.emptyLayoutArea;
                break;
            }

            case 'link': {
                elem = dashboard.linkImageBox;
                draggedElem = dashboard.emptyLayoutArea;
                break;
            }

            default: {
                reject('Widget does not exist')
                break;
            }
        }

        Wait.upTo(waitTime).until(elem, Is.visible())
        await BrowseTheWeb.as(damjan).locateAll(elem).browser_.driver.actions()
            .mouseDown(BrowseTheWeb.as(damjan).locate(elem))
            .mouseMove(BrowseTheWeb.as(damjan).locate(dashboard.emptyLayoutArea))
            .mouseUp(BrowseTheWeb.as(damjan).locate(dashboard.emptyLayoutArea))
            .perform()
        Wait.upTo(waitTime).until(draggedElem, Is.visible());
        resolve();
    }).catch(err => { reject(err) })

}
// TODO Edit to return Task object
export const selectWidgetType = async (widgetToSelect) => {
    return new Promise(async (resolve, reject) => {
        let elem;
        switch (widgetToSelect) {

            case 'pressure': {
                elem = dashboard.pressureWidget;
                break;
            }

            case 'horizontal column': {
                elem = dashboard.horizontalColumnWidgetButton;
                break;
            }

            case 'vertical column': {
                elem = dashboard.verticalColumnWidget;
                break;
            }

            case 'run time': {
                elem = dashboard.runTimeWidget;
                break;
            }

            case 'label': {
                elem = dashboard.labelWidget;
                break;
            }

            case 'status': {
                elem = dashboard.statusWidget;
                break;
            }

            default: {
                reject('Widget does not exist')
                break;
            }
        }

        Wait.upTo(waitTime).until(elem, Is.clickable())
        Scroll.to(elem)
        await BrowseTheWeb.as(damjan).locateAll(elem).click();
        resolve();
    }).catch(err => { reject(err) })
}