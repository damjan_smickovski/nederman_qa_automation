import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element, ElementArrayFinder, ElementFinder } from 'protractor';
export { by, element }

export const dashboard = {

    //General dashboard elements
    damjanTestDashboard: Target.the('Damjan test dashboard').located(by.cssContainingText('.template-name', '1 Damjan Blank')),
    dashboardMoreOptionsButton: Target.the('Dashboard more options button').located(by.xpath(`//*[@id="page-content"]/app-my-dashboard/div/div/div[2]/div/div[2]/div/app-menu-buttons/div/button`)),
    dashboardEditButton: Target.the('Dashboard edit button').located(by.cssContainingText('.btn', 'Edit')),

    //Layouts Tab
    layoutsButton: Target.the('Layout button').located(by.cssContainingText('.nav', 'Layout')),
    createLayoutButton: Target.the('Create layout').located(by.cssContainingText('.btn', 'Create layout')),
    size160pxLayoutRadioButton: Target.the('160px radio button').located(by.css('#page-content > app-my-dashboard > div > div > app-render > div > div > app-nm-report-wrapper > div > div > app-nm-report-row > modal-report-row > div.modal.show.fade.in.ng-trigger.ng-trigger-toggleModal > div > div > div.modal-body > row-height-component > div > div.wrapper > div > div:nth-child(3) > label')),
    oneColumnLayout: Target.the('One column layout').located(by.xpath('//*[@id="layout"]/div[1]/div/div/div')),
    draggedLayoutArea: Target.the('Drag layout area').located(by.css('#page-content > app-my-dashboard > div > div > app-render > div > div > app-nm-report-wrapper')),
    emptyLayoutArea: Target.the('Empty layout area').located(by.xpath('//*[@id="page-content"]/app-my-dashboard/div//app-nm-report-col/div')),

    //Components Tab
    componentsButton: Target.the('Components button').located(by.xpath(`//span[contains(text(),'Comp')]`)),
    superChartImageBox: Target.the('Super chart box from top menu').located(by.xpath(`//*[@src="assets/images/super-chart.svg"]`)),
    superTableImageBox: Target.the('Super table box from top menu').located(by.xpath(`//*[@src="assets/images/super-table.svg"]`)),
    linkImageBox: Target.the('Link box from top menu').located(by.xpath(`//*[@src="assets/images/link.svg"]`)),
  
    //Blank / Empty area elements
    superWidgetImageBox: Target.the('Super widget box from top menu').located(by.xpath(`//*[@src = "assets/images/super-widget.svg"]`)),
    draggedSupperWidget: Target.the('Dragged Super widget box').located(by.xpath(`//*[@src="assets/images/no-sensor-attached.svg"]`)),
    editWidgetButton: Target.the('Edit widget').located(by.xpath(`//*[@src="assets/images/edit-component.svg"]`)),

    //Edit component
    pressureWidget: Target.the('Pressure column widget').located(by.xpath(`//*/img[@src="assets/images/pressure.svg"]`)),
    horizontalColumnWidgetButton: Target.the('Horizontal column widget').located(by.xpath(`//*/img[@src="assets/images/horizontal.svg"]`)),
    verticalColumnWidget: Target.the('Vertical column widget').located(by.xpath(`//*/img[@src="assets/images/vertical.svg"]`)),
    runTimeWidget: Target.the('Run time widget').located(by.xpath(`//*/img[@src="assets/images/runtime.svg"]`)),
    labelWidget: Target.the('Label widget').located(by.xpath(`//*/img[@src="assets/images/label.svg"]`)),
    statusWidget: Target.the('Status widget').located(by.xpath(`//*/img[@src="assets/images/status.svg"]`)),

    //Component general fields

    labelField: Target.the('Label(Title) field').located(by.xpath(`//*/input[@formcontrolname="name"]`)),
    descriptionField: Target.the('Label(Title) field').located(by.xpath(`//*/input[@formcontrolname="description"]`)),
    selectButtonSensor: Target.the('Button sensor from the dropdown').located(by.cssContainingText('option', 'button')),
    
    //Ovveride elements
    overrideSensorCheckbox: Target.the('Ovverride sensor checkbox').located(by.xpath(`//*/input[@id="override"]`)),
    minValueField: Target.the('Min value field').located(by.id(`metadataMin`)),
    maxValueField: Target.the('Min value field').located(by.id(`metadataMax`)),
    selectHzUnit: Target.the('Hz unit from the dropdown').located(by.cssContainingText('option', 'Hz')),
    addRangeButton: Target.the('Add range button').located(by.xpath(`//*/span[@class="add-new-attribute"]`)),
    
    //Range 1 elements
    range1LabelField: Target.the('Range 1 label field').located(by.xpath(`//*[@formarrayname="ranges"]/div[1]/div[1]/input`)),
    toField1: Target.the('To range field').located(by.xpath(`//*[@formarrayname="ranges"]/div[1]/div[3]`)),

    //Range 2 elements
    range2LabelField: Target.the('Range 2 label field').located(by.xpath(`//*[@formarrayname="ranges"]/div[2]/div[1]/input`)),
    fromField2: Target.the('From range field').located(by.xpath(`//*[@formarrayname="ranges"]/div[2]/div[2]`)),
    toField2: Target.the('To range field').located(by.xpath(`//*[@formarrayname="ranges"]/div[2]/div[3]`)),

    addToDashboardButton: Target.the('Add to dashboard button').located(by.xpath(`//*/button[@type="submit"]`)),
    removeWidgetButton: Target.the('Remove widget from layout').located(by.xpath(`//*/i[@class="fa fa-times"]`)),




};




