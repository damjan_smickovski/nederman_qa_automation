import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element } from 'protractor';
import { TargetLocator } from 'selenium-webdriver';

export const ruleEngine = {

    //Rule engine page
    addNewRuleButton:  Target.the('Add New button').located(by.id('addNewDashboard')),
    searchByNameField:    Target.the('Search name field').located(by.id('searchSub')),
    ruleList:    Target.the('Rule list').located(by.xpath('//div[@class="tableWrap col-md-12"]')),


    // Rule dropdown details
    nedermanHoldingCompanyDropdown:    Target.the('Nederman holding Company from dropdown menu').located(by.cssContainingText('#customerId > option', 'Nederman Holding AB')),
    skopjePlantDropdown:    Target.the('Skopje Plant from the dropdown menu').located(by.cssContainingText('#plantId > option','Skopje')),
    skopjeSystemDropdown:    Target.the('Skopje System from dropdown menu').located(by.cssContainingText('#systemId > option','Skopje Test')),
    //Rule type and text fields
    ruleNameField:    Target.the('Rule Name field').located(by.id('name')),
    ruleDescField:    Target.the('Rule desc field').located(by.id('description')),
    alarmTypeCRadioButton:  Target.the('C radio button').located(by.xpath('*//label[@for="c"]')),
    alarmTypeBRadioButton:  Target.the('B radio button').located(by.xpath('*//label[@for="b"]')),
    alarmTypeARadioButton:  Target.the('A radio button').located(by.xpath('*//label[@for="a"]')),
    // Conditions fields
    addConditionButton:    Target.the('Add condition button').located(by.xpath('//*[@id="expression"]//button[1]')),
    
    //Condition sensor options
    buttonSensorConditionDropdown:    Target.the('Button sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='button'][1]`)),
    lastSyncSensorConditionDropdown:    Target.the('Last sync sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='lastsync'][1]`)),
    lightLeftSensorConditionDropdown:    Target.the('Light left sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='light_left'][1]`)),
    lightRightSensorConditionDropdown:    Target.the('Light right sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='light_right'][1]`)),
    temperatureSensorConditionDropdown:    Target.the('Temperature sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='temperature'][1]`)),
    watchdogSensorConditionDropdown:    Target.the('Watchdog sensor from the condition sensor dropdown').located(by.xpath(`//div[@class="col-md-9 relative-div conditions-group"]//option[text()='watchdog'][1]`)),

    //Operator options
    equalsOperatorConditionDropdown:    Target.the('equals operator from the condition button').located(by.xpath(`*//option[text()='=']`)),
    lessThanOperatorConditionDropdown:    Target.the('less than operator from the condition button').located(by.xpath(`*//option[text()='<']`)),
    lessThanOrEqualsOperatorConditionDropdown:    Target.the('less than or equals operator from the condition button').located(by.xpath(`*//option[text()='<=']`)),
    moreThanOperatorConditionDropdown:    Target.the('more than operator from the condition button').located(by.xpath(`*//option[text()='<=']`)),
    moreThanOrEqualsOperatorConditionDropdown:    Target.the('more than or equals operator from the condition button').located(by.xpath(`*//option[text()='>=']`)),
    moreOrLessOperatorConditionDropdown:    Target.the('more than or less than operator from the condition button').located(by.xpath(`*//option[text()='<>']`)),

    //Additional conditions
    conditionValueField:    Target.the('Condition value field').located(by.xpath('*//div[@class="group-conditions"]//input[@type="number"]')),
    inAlarmField:    Target.the('In alarm field').located(by.id('alarmTimeout')),
    outAlarmField:    Target.the('Out alarm field').located(by.id('clearTimeout')),
    correctiveActionCheckbox:    Target.the('Corrective action').located(by.id('ca')),
    autoCloseField:    Target.the('Auto close alarm field').located(by.id('autoClose')),

    // Priority and subscribers fields
    buttonValueSensorDropdown:    Target.the('Select value button sensor dropdown').located(by.xpath(`//select[@formcontrolname="valueSensor"]//option[contains(text(), 'button')]`)),
    lightLeftValueSensorDropdown:    Target.the('Select value light left sensor dropdown').located(by.xpath(`//select[@formcontrolname="valueSensor"]//option[contains(text(), 'light_left')]`)),
    lightRightValueSensorDropdown:    Target.the('Select value light right sensor dropdown').located(by.xpath(`//select[@formcontrolname="valueSensor"]//option[contains(text(), 'light_right')]`)),
    temperatureValueSensorDropdown:    Target.the('Select value temperature sensor dropdown').located(by.xpath(`//select[@formcontrolname="valueSensor"]//option[contains(text(), 'temperature')]`)),



    mediumPriorityRadioButton:    Target.the('Medium priority radio button').located(by.xpath(`//label[@for="medium"]`)),
    lowPriorityRadioButton:    Target.the('Medium priority radio button').located(by.xpath(`//label[@for="low"]`)),
    highPriorityButton:    Target.the('Medium priority radio button').located(by.xpath(`//label[@for="high"]`)),
    subscriberField:    Target.the('Subscriber field').located(by.id(`searchSub`)),
    testUserCheckbox:    Target.the('Test user checkbox').located(by.id(`chkbox4`)),
    enableRuleCheckbox:    Target.the('Enable rule checkbox').located(by.id(`chkbox3`)),
    saveRuleButton:    Target.the('Save rule button').located(by.id(`addPlant`)),

};
