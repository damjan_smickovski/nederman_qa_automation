import { expect } from 'chai';
import { browser, by, element, promise, protractor } from 'protractor';
import { reject } from 'q';
import { Actor, Click, Enter, Is, Scroll, See, Task, Text, Wait } from 'serenity-js/lib/screenplay-protractor';
import { BrowseTheWeb, Duration, Select, Target, Clear } from 'serenity-js/lib/serenity-protractor';
import { equals } from '../assertions';
import { ruleEngine } from './ui/ruleEngineElements';
const waitTime = Duration.ofMillis(30000);

export const addNewRule = ({
    clickAddNewRuleButton: () => Task.where(`#actor clicks the Add New rule button`,

        Wait.upTo(waitTime).until(ruleEngine.addNewRuleButton, Is.visible()),
        Scroll.to(ruleEngine.addNewRuleButton),
        Click.on(ruleEngine.addNewRuleButton),

    ),
    enterRuleName: (ruleName: string) => Task.where(`#actor adds the Rule name`,

        Wait.upTo(waitTime).until(ruleEngine.ruleNameField, Is.visible()),
        Scroll.to(ruleEngine.ruleNameField),
        Enter.theValue(ruleName).into(ruleEngine.ruleNameField),

    ),

    enterRuleDescription: (ruleDesc: string) => Task.where(`#actor enters the Rule Description`,

        Wait.upTo(waitTime).until(ruleEngine.ruleDescField, Is.visible()),
        Scroll.to(ruleEngine.ruleDescField),
        Enter.theValue(ruleDesc).into(ruleEngine.ruleDescField),

    ),

    selectAlarmType: (priorityToSelect: string) => {
        let elem: Target;
        switch (priorityToSelect) {
            case 'A':
                elem = ruleEngine.alarmTypeARadioButton;
                break;
            case 'B':
                elem = ruleEngine.alarmTypeBRadioButton;
                break;
            case 'C':
                elem = ruleEngine.alarmTypeCRadioButton;
                break;
        }

        return Task.where(`#actor selects the ${priorityToSelect} alarm type`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem)
        );
    },

    selectCompany: (companyName: string) => {
        let elem: Target;
        switch (companyName) {
            case 'Nederman Holding AB':
                elem = ruleEngine.nedermanHoldingCompanyDropdown;
                break;
        }
        return Task.where(`#actor clicks the alarm type C`,

            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },
    selectRuleSystem: (systemName: string) => {
        let elem = Target.the(`${systemName} system`).located(by.xpath(`//option[text()="${systemName}"]`));
        return Task.where(`#actor selects the ${systemName} System`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    selectPlant: (plantName: string) => {
        let elem: Target;
        switch (plantName) {
            case 'Skopje':
                elem = ruleEngine.skopjePlantDropdown;
                break;
        }
        return Task.where(`#actor selects the Skopje Test plant`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),

        )
    },

    clickAddConditionButton: () => Task.where(`#actor clicks the add condition button`,

        Wait.upTo(waitTime).until(ruleEngine.addConditionButton, Is.clickable()),
        Scroll.to(ruleEngine.addConditionButton),
        Click.on(ruleEngine.addConditionButton),

    ),

    selectSensorFromDropdown: (sensorName: String) => {
        let elem: Target;
        switch (sensorName) {
            case 'button':
                elem = ruleEngine.buttonSensorConditionDropdown;
                break;

            case 'light_left':
                elem = ruleEngine.lightLeftSensorConditionDropdown;
                break;

            case 'light_right':
                elem = ruleEngine.lightRightSensorConditionDropdown;
                break;

            case 'temperature':
                elem = ruleEngine.temperatureSensorConditionDropdown;
                break;
        }

        return Task.where(`#actor selects the ${sensorName} from condition dropdown`,
            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),

        )
    },

    slectOperatorFromDropdown: (operatorValue: String) => {
        let elem: Target;

        switch (operatorValue) {
            case '=':
                elem = ruleEngine.equalsOperatorConditionDropdown
                break;

            case '<':
                elem = ruleEngine.lessThanOperatorConditionDropdown
                break;

            case '>':
                elem = ruleEngine.moreThanOperatorConditionDropdown
                break;

            case '<=':
                elem = ruleEngine.lessThanOrEqualsOperatorConditionDropdown
                break;

            case '>=':
                elem = ruleEngine.moreThanOrEqualsOperatorConditionDropdown
                break;

            case '<>':
                elem = ruleEngine.moreOrLessOperatorConditionDropdown
                break;
        }

        return Task.where(`#actor selects the ${operatorValue} from condition dropdown`,

            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),
        )
    },

    enterValueInConditionField: (conditionValue: string) => Task.where(`#actor enters the value in the condition field`,

        Wait.upTo(waitTime).until(ruleEngine.conditionValueField, Is.visible()),
        Scroll.to(ruleEngine.conditionValueField),
        Enter.theValue(conditionValue).into(ruleEngine.conditionValueField),

    ),


    enterValueInTheInAlarmField: (inAlarmTime: string) => Task.where(`#actor enters the value in the in alarm field`,

        Wait.upTo(waitTime).until(ruleEngine.inAlarmField, Is.visible()),
        Scroll.to(ruleEngine.inAlarmField),
        Enter.theValue(inAlarmTime).into(ruleEngine.inAlarmField),

    ),

    enterValueInTheOutAlarmField: (outAlarmTime: string) => Task.where(`#actor enters the value in the out alarm field`,

        Wait.upTo(waitTime).until(ruleEngine.outAlarmField, Is.visible()),
        Scroll.to(ruleEngine.outAlarmField),
        Enter.theValue(outAlarmTime).into(ruleEngine.outAlarmField),

    ),

    enterAlarmAutoCloseTime: (autoCloseTime: string) => Task.where(`#actor enters the value in the auto close field`,

    Wait.upTo(waitTime).until(ruleEngine.autoCloseField, Is.visible()),
    Scroll.to(ruleEngine.autoCloseField),
    Clear.theValueOf(ruleEngine.autoCloseField),
    Enter.theValue(autoCloseTime).into(ruleEngine.autoCloseField),

),

    selectValueSensorFromDropdown: (valueSensorname) => {
        let elem: Target;

        switch (valueSensorname) {
            case 'button':
                elem = ruleEngine.buttonValueSensorDropdown;
                break;

            case 'light_left':
                elem = ruleEngine.lightLeftValueSensorDropdown;
                break;

            case 'light_right':
                elem = ruleEngine.lightRightValueSensorDropdown;
                break;

            case 'temperature':
                elem = ruleEngine.temperatureValueSensorDropdown;
                break;
        }

        return Task.where(`#actor selects the ${valueSensorname} sensor from the value sensor dropdown`,

            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),

        )
    },

    selectPriority: (priorityType: string) => {
        let elem: Target;
        switch (priorityType) {
            case 'low':
                elem = ruleEngine.lowPriorityRadioButton;
                break;

            case 'medium':
                elem = ruleEngine.mediumPriorityRadioButton;
                break;

            case 'high':
                elem = ruleEngine.highPriorityButton;
                break;
        }

        return Task.where(`#actor selects the ${priorityType} priority radio button`,

            Wait.upTo(waitTime).until(elem, Is.clickable()),
            Scroll.to(elem),
            Click.on(elem),

        )
    },

    enterValueInSubscriberField: (subscriberString: string) => Task.where(`#actor enters the value in the subscriber field`,

        Wait.upTo(waitTime).until(ruleEngine.subscriberField, Is.visible()),
        Scroll.to(ruleEngine.subscriberField),
        Enter.theValue(subscriberString).into(ruleEngine.subscriberField),
    ),

    selectTestUserSubscriber: () => Task.where(`#actor selects the test user subscriber`,

        Wait.upTo(waitTime).until(ruleEngine.testUserCheckbox, Is.clickable()),
        Scroll.to(ruleEngine.testUserCheckbox),
        Click.on(ruleEngine.testUserCheckbox),

    ),

    tickTheEnableRuleCheckbox: () => Task.where(`#actor checks the enable rule checkbox`,

        Wait.upTo(waitTime).until(ruleEngine.enableRuleCheckbox, Is.clickable()),
        Scroll.to(ruleEngine.enableRuleCheckbox),
        Click.on(ruleEngine.enableRuleCheckbox),

    ),

    tickCorrectiveActionCheckbox: () => Task.where(`#actor checks the corrective action checkbox`,

    Wait.upTo(waitTime).until(ruleEngine.correctiveActionCheckbox, Is.clickable()),
    Scroll.to(ruleEngine.correctiveActionCheckbox),
    Click.on(ruleEngine.correctiveActionCheckbox),

),

    clickSaveRuleButton: () => Task.where(`#actor clicks the save rule button`,

        Wait.upTo(waitTime).until(ruleEngine.saveRuleButton, Is.clickable()),
        Scroll.to(ruleEngine.saveRuleButton),
        Click.on(ruleEngine.saveRuleButton),
    ),

    checkIfRuleWasCreated: (ruleToCheck: string) => {
        let ruleName = Target.the('new string').located(by.xpath(`//div[@class="tableWrap col-md-12"]//span[text()="${ruleToCheck}"]`))

        return Task.where(`#actor checks if the rule was created`,
            Wait.upTo(waitTime).until(ruleName, Is.present()),
            Scroll.to(ruleName)
        );
    },

    clickOnRuleFromRuleList: (ruleToClick: string) => {
        let ruleName = Target.the('new string').located(by.xpath(`//div[@class="tableWrap col-md-12"]//span[text()="${ruleToClick}"]`))

        return Task.where(`#actor clicks the rule`,
            Wait.upTo(waitTime).until(ruleName, Is.clickable()),
            Scroll.to(ruleName),
            Click.on(ruleName)
        );
    }

});
