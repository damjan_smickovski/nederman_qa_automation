import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element } from 'protractor';
import { TargetLocator } from 'selenium-webdriver';

export const alarmCenterPageElements = {
    searchField:    Target.the('Search field element').located(by.id(`alarmByName`)),
    handledAlarmsOption:    Target.the('Handled alarms optin').located(by.xpath(`//select[@id="alarmBySystem"]//option[text()="Handled alarms"]`)),
};
