import { expect } from 'chai';
import { browser, by, element, promise, protractor, ProtractorBrowser } from 'protractor';
import { reject } from 'q';
import { Actor, Click, Enter, Is, Scroll, See, Task, Text, Wait } from 'serenity-js/lib/screenplay-protractor';
import { BrowseTheWeb, Duration, Select, Target, Execute } from 'serenity-js/lib/serenity-protractor';
import { equals } from '../assertions';
import { alarmCenterPageElements } from './ui/alarmCenterPageElements';
const waitTime = Duration.ofMillis(30000);
const damjan = Actor.named('damjan').whoCan(BrowseTheWeb.using(protractor.browser));
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';


export const alarmCenterPageActions = ({

    checkAlarmStatus: (alarmName: string, statusCheck: string) => {
        let elem: Target;
        switch (statusCheck) {
            case 'Handled':
                elem = Target.the("Status field of the element").located(by.xpath(`//div[@class="tableWrap col-md-12"]//span[text()="${alarmName}"]/parent::*/parent::*//span[text()="Handled"]`));
                break;
            case 'Acknowledged':
                elem = Target.the("Status field of the element").located(by.xpath(`//div[@class="tableWrap col-md-12"]//span[text()="${alarmName}"]/parent::*/parent::*//span[text()="Acknowledged"]`));
                break;
        }
        return Task.where(`#actor checks the alarm status`,
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            See.if(Text.of(elem), equals(statusCheck)),
        )
    },

    enterValueInSearchField: (searchValue: string) => Task.where(`#actor enters the value in the alarm center search field`,
        Wait.upTo(waitTime).until(alarmCenterPageElements.searchField, Is.visible()),
        Scroll.to(alarmCenterPageElements.searchField),
        Enter.theValue(searchValue).into(alarmCenterPageElements.searchField),
    ),


    selectAlarmStatus: (alarmStatus: string) => {
        let elem: Target;
        switch (alarmStatus) {

            case 'Handled':
                elem = alarmCenterPageElements.handledAlarmsOption;
                break;
        }
        return Task.where(`#actor checks the alarm status`,
            Wait.upTo(waitTime).until(elem, Is.visible()),
            Scroll.to(elem),
            Click.on(elem)
        )
    },



});
