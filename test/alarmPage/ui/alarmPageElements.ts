import { Target } from 'serenity-js/lib/screenplay-protractor';
import { by, element } from 'protractor';
import { TargetLocator } from 'selenium-webdriver';

export const alarmPageElements = {
    alarmTitle:    Target.the('Alarm title element').located(by.css(`#page-content > app-alarm-details > div > div:nth-child(1) > div.priority.col-md-7.alarm-name`)),
    acknowledgeButton:    Target.the('Alarm acknowledge button').located(by.id(`acknowledgeAlarm`)),
    handleButton:    Target.the('Alarm handle button').located(by.id(`handleAlarm`)),
    addNewAction:    Target.the('Alarm handle button').located(by.xpath(`//a[text()="add new corrective action"]`)),
    autoCloseText: Target.the("Auto close element text").located(by.xpath(`//em[contains(text(),"Alarm auto-closed")]`))

};
