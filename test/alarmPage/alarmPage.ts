import { alarmCenterPageElements } from './../alarmCenterPage/ui/alarmCenterPageElements';

import { browser, by, element, promise, protractor, ProtractorBrowser } from 'protractor';
import { Actor, Click, Enter, Is, Scroll, See, Task, Text, Wait } from 'serenity-js/lib/screenplay-protractor';
import { BrowseTheWeb, Duration, Select, Target, Execute } from 'serenity-js/lib/serenity-protractor';
import { equals } from '../assertions';
import { alarmPageElements } from './ui/alarmPageElements';
const waitTime = Duration.ofMillis(30000);
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';


export const alarmActions = ({

    checkAlarmName: (nameToCheck: string) => Task.where(`#actor checks the alarm title`,
        Wait.upTo(waitTime).until(alarmPageElements.alarmTitle, Is.visible()),
        // Scroll.to(alarmPageElements.alarmTitle),
        Wait.for(Duration.ofSeconds(10)),
        See.if(Text.of(alarmPageElements.alarmTitle), equals(nameToCheck)),
    ),

    clickAcknowledgeButton: () => Task.where(`#actor clicks the acknowledge button`,
        Wait.upTo(waitTime).until(alarmPageElements.acknowledgeButton, Is.clickable()),
        Scroll.to(alarmPageElements.acknowledgeButton),
        Click.on(alarmPageElements.acknowledgeButton)

    ),

    clickHandleButton: () => Task.where(`#actor clicks the handle button`,
        Wait.upTo(waitTime).until(alarmPageElements.handleButton, Is.clickable()),
        Scroll.to(alarmPageElements.handleButton),
        Click.on(alarmPageElements.handleButton)
    ),

    clickAddCorectionButton: () => Task.where(`#actor clicks the add correction button`,
        Wait.upTo(waitTime).until(alarmPageElements.addNewAction, Is.clickable()),
        Scroll.to(alarmPageElements.addNewAction),
        Click.on(alarmPageElements.addNewAction)
    ),

    checkIfAcknowledgeTimestampIsCorrect: () => {
        var now = new Date();
        var utc = new Date(now.getTime() + now.getTimezoneOffset());
        let time;
        let minutes: any = utc.getMinutes();
        minutes = minutes.toString();
        if (utc.getMinutes() < 10)
            minutes = '0' + minutes;

        time = `${utc.getHours()}:${minutes}`;
        let elem: Target = Target.the("Acknowledge timestamp").located(by.xpath(`//b[contains(text(),"${time}")]`));
        logger.debug(`Current timestamp: ${time}`);
        return Task.where(`#checks the timestamp of the acknowledgment`,
            Wait.upTo(waitTime).until(elem, Is.visible()),
        )
    },


    checkIfAlarmWasAutoClosed: () => { 
        return Task.where(`#checks the timestamp of the acknowledgment`,
            Wait.upTo(waitTime).until(alarmPageElements.autoCloseText, Is.visible()),
            Scroll.to(alarmPageElements.autoCloseText),
        )
    }






});
