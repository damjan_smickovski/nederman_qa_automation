import { alarmCenterPageActions } from './../../test/alarmCenterPage/alarmCenterPage';
import { helpers, getDataFromRuleFile, killProcess } from '../../scripts/helpers'
import { browser } from 'protractor';

export = function ruleEngineSteps() {
    this.Then(/^the alarm status should be "([^"]*)"$/, async function (alarmStatus: string) {
        let ruleName = await helpers.getLatestEntryFromRuleIdFile();
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmCenterPageActions.checkAlarmStatus(ruleName, alarmStatus)
        );
    });

    this.When(/^he selects the "([^"]*)" alarm status from the dropdown$/, async function (alarmStatus: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmCenterPageActions.selectAlarmStatus(alarmStatus)
        );
    });
};
