import { clickMoreOptionsButton, clickEditButton, dragAndDropLayout, clickCreateLayout } from './../../test/dashboard/dashboard';
import { Open } from 'serenity-js/lib/screenplay-protractor';
import { clickActionCenter, clickAlarmCenter,   clickDashboards, clickUserPreferences, clickLanguage, clickSetAsEntryPoint, clickDotPreferences, clickLogOut, checkIfYouSignedOutOfYourAccountTextIsVisible, clickAdministration, clickLocation, clickSystems, clickEdit } from './../../test/homeScreen/homeScreen';
import { checkIfInsightLogoIsVisible, clickOk } from './../../test/login/login';

import { clickNext, clickSignIn, enterEmail, enterPassword, reduceLoginNo } from '../../test/login/login';

import { selectDamjanTestDashboard } from '../../test/dashboard/dashboard';
var loggedIn = false;
export = function loginSteps() {

    this.Given(/^(.*) navigates to the Nederman environment$/, async function (actor: string) {
        return this.stage.theActorCalled(actor).attemptsTo(
            Open.browserOn('https://test.iot.n5-nederman.com/'),
        );

    });

    this.When(/^he enters his email "([^"]*)"$/, function (email: string) {
        if (loggedIn === false) {
            return this.stage.theActorInTheSpotlight().attemptsTo(
                enterEmail.of(email),
            );
        }

        else
            return;
    });

    this.When(/^he enters his password "([^"]*)"$/, function (password: string) {
        if (loggedIn === false)
            return this.stage.theActorInTheSpotlight().attemptsTo(
                enterPassword.of(password),
            );
        else
            return
    });

    this.When(/^he clicks the next button$/, function () {
        if (loggedIn === false)
            return this.stage.theActorInTheSpotlight().attemptsTo(
                clickNext.of(),
            );
        else
            return
    });

    this.When(/^he clicks the sign in button$/, function () {
        if (loggedIn === false)
            return this.stage.theActorInTheSpotlight().attemptsTo(
                clickSignIn.of(),
            );

        else
            return;
    });

    this.When(/^he clicks no on the reduce login times$/, function () {
        if (loggedIn === false)
            return this.stage.theActorInTheSpotlight().attemptsTo(
                reduceLoginNo.of(),
            );
        else
            return;

    });

    this.Then(/^he should be logged in$/, async function () {
        loggedIn = true;
        return this.stage.theActorInTheSpotlight().attemptsTo(
            checkIfInsightLogoIsVisible.of(),
        );

    });


    this.When(/^he clicks the OK button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickOk.of(),
        );

    });
    this.Then(/^he should be logged out$/, async function () {
        loggedIn = false;
        return this.stage.theActorInTheSpotlight().attemptsTo(
            checkIfYouSignedOutOfYourAccountTextIsVisible.of(),
        );

    });

};
