import { Open } from 'serenity-js/lib/screenplay-protractor';
import { addNewRule } from './../../test/ruleEngine/ruleEngine';
import { helpers, getDataFromRuleFile } from '../../scripts/helpers'
import { browser } from 'protractor';
var log4js = require('log4js');
var logger = log4js.getLogger();


let ruleNameVar: string;

export = function ruleEngineSteps() {

    this.When(/^he enters the rule name "([^"]*)"$/, function (ruleName: string) {
        const uuid = require("uuid");
        var id = uuid.v4();
        ruleName += ` - ${id}`;
        ruleNameVar = ruleName;
        helpers.addEntryToRuleData(ruleNameVar);
        delete require.cache[require.resolve('uuid')]
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterRuleName(ruleName),
        );
    });

    this.When(/^he enters the rule desc "([^"]*)"$/, function (ruleDesc: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterRuleDescription(ruleDesc),
        );
    });
    this.When(/^he selects the alarm type "([^"]*)"$/, function (typeToselect: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectAlarmType(typeToselect),
        );
    });

    this.When(/^he selects "([^"]*)" from the company dropdown$/, function (companyName: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectCompany(companyName),
        );

    });

    this.When(/^he selects the "([^"]*)" plant$/, function (plantName: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectPlant(plantName),
        );

    });

    this.When(/^he selects the "([^"]*)" system$/, async function (systemName: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectRuleSystem(systemName),
        );
    });

    this.When(/^he clicks the add condition button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.clickAddConditionButton(),
        );
    });

    this.When(/^he sets the condition sensor to "([^"]*)"$/, async function (sensorName: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectSensorFromDropdown(sensorName),
        );
    });

    this.When(/^he sets the sensor operator to "([^"]*)"$/, async function (operatorValue: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.slectOperatorFromDropdown(operatorValue),
        );
    });

    this.When(/^he sets the sensor value to "([^"]*)"$/, async function (sensorValue: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterValueInConditionField(sensorValue),
        );
    });

    this.When(/^he sets the in alarm time as "([^"]*)"$/, async function (inAlarmValue) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterValueInTheInAlarmField(inAlarmValue),
        );
    });

    this.When(/^he sets the out alarm time as "([^"]*)"$/, async function (outAlarmValue) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterValueInTheOutAlarmField(outAlarmValue),
        );
    });

    this.When(/^he sets the auto close time as "([^"]*)"$/, async function (autoCloseValue) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterAlarmAutoCloseTime(autoCloseValue),
        );
    });

    this.When(/^he selects the "([^"]*)" sensor from the sensor value dropdown$/, async function (valueSensorName) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectValueSensorFromDropdown(valueSensorName),
        );
    });

    this.When(/^he sets the "([^"]*)" priority$/, async function (priorityType) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.selectPriority(priorityType),
        );
    });

    this.When(/^he sets the subscriber as "([^"]*)"$/, async function (subscriberName) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.enterValueInSubscriberField(subscriberName),
            addNewRule.selectTestUserSubscriber()
        );
    });

    this.When(/^he enables the rule$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.tickTheEnableRuleCheckbox(),
        );

    });

    this.When(/^he saves the rule$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.clickSaveRuleButton(),
        );

    });

    this.Then(/^the rule should be created$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.checkIfRuleWasCreated(ruleNameVar),
        );
    });

    this.When(/^he clicks on the "([^"]*)" rule$/, async function (ruleToClick) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.clickOnRuleFromRuleList(ruleNameVar),
        );
    });

    this.When(/^he clicks on the corrective action checkbox$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            addNewRule.tickCorrectiveActionCheckbox(),
        );
    });

    this.When(/^he he waits for "([^"]*)" seconds$/, async function (timeToWait: number) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            helpers.hardWait(timeToWait),
        );
    });

    this.When(/^mock data succesfully generated as SERIES: "([^"]*)", FROM: "([^"]*)", TO: "([^"]*)", NAME: "([^"]*)"$/, async function (series: string, from: Date, to: Date, name: string) {
        return await helpers.createDumpData(series, from, to, name);
    });

    this.When(/^he runs the mocked data file "([^"]*)"$/, async function (outputFileName: string) {
        helpers.runMockData(outputFileName);
        return;
    });

    this.When(/^he navigates to the rule link from his email$/, async function () {
        delete require.cache[require.resolve('../../scripts/emailListener.js')];
        const emaillistener = require('../../scripts/emailListener.js');
        let rule = await helpers.getLatestEntryFromRuleIdFile();
        let link = await emaillistener.fetchMessages(ruleNameVar).catch(err => Promise.reject(err));
       // logger.debug(`Found link "${link}"`);
        await emaillistener.closeClient();
        await browser.get(link);
    });
};
