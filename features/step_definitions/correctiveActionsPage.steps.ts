import { correctiveActions } from './../../test/correctiveActionPage/correctiveActions';
import { helpers, getDataFromRuleFile, killProcess } from '../../scripts/helpers'

export = function ruleEngineSteps() {
    this.When(/^he enters "([^"]*)" as the CA name$/, async function (CAName: string) {
        const uuid = require("uuid");
        var id = uuid.v4();
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.enterCAName(CAName + ' - ' + id)
        );
    });

    this.When(/^he selects "([^"]*)" as the CA type$/, async function (CAType: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.selectCAType(CAType)
        );
    });

    this.When(/^selects the start date$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.selectStartDate()
        );
    });

    this.When(/^selects the end date$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.selectEndDate()
        );
    });

    this.When(/^he enters "([^"]*)" as the CA cause$/, async function (CACause: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.enterCACause(CACause)
        );
    });

    this.When(/^he enters "([^"]*)" as the CA response$/, async function (CAResponse: string) {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.enterCAResponse(CAResponse)
        );
    });

    this.When(/^he clicks the add CA button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            correctiveActions.clickAddButton()
        );
    });

};
