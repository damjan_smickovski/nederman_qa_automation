import {
    checkIfYouSignedOutOfYourAccountTextIsVisible, clickActionCenter, clickAddNew, clickAdministration,
    clickAlarmCenter, 
    clickDashboards, clickDotPreferences, clickEdit,
    clickLanguage, clickLocation, clickLogOut, 
     clickRuleEngine, 
     clickSetAsEntryPoint, clickSystems,
      clickThings, clickUserPreferences, selectNedermanHoldingFromTenantDropdown
 
} from './../../test/homeScreen/homeScreen';
import { killProcess } from '../../scripts/helpers';

var {After, Before} = require('cucumber');

export = function homePageSteps() {

    this.After(function () {
     
        killProcess();
      });

    this.When(/^he selects Nederman Holding from the tenant dropdown$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            selectNedermanHoldingFromTenantDropdown.of(),
        );

    });

    this.When(/^he clicks the alarm center button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickAlarmCenter.of(),
        );

    });

    this.When(/^he clicks the action center button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickActionCenter.of(),
        );

    });

    this.When(/^he clicks the dashboards button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickDashboards.of(),
        );

    });
    this.When(/^he clicks the user preferences button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickUserPreferences.of(),
        );

    });

    this.When(/^he clicks the language button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickLanguage.of(),
        );

    });
    this.When(/^he clicks the Set as entry point button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickSetAsEntryPoint.of(),
        );

    });
    this.When(/^he clicks the Dot preferences button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickDotPreferences.of(),
        );

    });

    this.When(/^he clicks the Log out button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickLogOut.of(),
        );

    });

    this.When(/^he clicks the administration button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickAdministration.of(),
        );

    });
    this.When(/^he clicks the location button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickLocation.of(),
        );

    });
    this.When(/^he clicks the systems button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickSystems.of(),
        );

    });
    this.When(/^he clicks the things button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickThings.of(),
        );

    });
    this.When(/^he clicks the rule engine button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickRuleEngine.of(),
        );

    });
    this.When(/^he clicks the add new button$/, async function() {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickAddNew.of(),
        );

    });

  
  
    
};
