import { clickMoreOptionsButton, clickEditButton, clickCreateLayout, clickLayoutsButton, dragAndDropWidget, dragAndDropLayout, selectThe160pxOption, clickComponentsButton, rightClickSuperWidget, editSuperWidget, selectWidgetType, fillOutSensorDetails, deleteWidget } from './../../test/dashboard/dashboard';
import { selectDamjanTestDashboard } from '../../test/dashboard/dashboard';


export = function dashboardSteps() {
    this.When(/^he clicks the damjan dashboard$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            selectDamjanTestDashboard.of(),
        );

    });

    this.When(/^he clicks the more options button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickMoreOptionsButton.of(),
        );
    });


    this.When(/^he clicks the edit button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            await clickEditButton.of(),
        );
    });

// TODO remove promise and replace with ActorInTheSpotlight
    this.When(/^he clicks the drag and drop button$/, async function () {
        return new Promise(async (resolve, reject) => {
            await dragAndDropLayout();
            resolve();
        });
    });
// TODO remove promise and replace with ActorInTheSpotlight
    this.When(/^he right clicks on the super widget$/, async function () {
        return new Promise(async (resolve, reject) => {
            await rightClickSuperWidget();
            resolve();
        });
    });

    this.When(/^he clicks the create layout button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickCreateLayout.of(),
        );
    });

    this.When(/^he clicks the layouts button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickLayoutsButton.of(),
        );
    });


    this.When(/^he clicks the components button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            clickComponentsButton.of(),
        );
    });

// TODO remove promise and replace with ActorInTheSpotlight
    this.When(/^he drags a "([^"]*)" to a layout$/, async function (widgetToDrag) {
        return new Promise(async (resolve, reject) => {
            await dragAndDropWidget(widgetToDrag);
            resolve();
        })
    });
// TODO remove promise and replace with ActorInTheSpotlight
    this.When(/^he clicks the "([^"]*)" widget$/, async function (widgetToSelect) {
        return new Promise(async (resolve, reject) => {
            await selectWidgetType(widgetToSelect);
            resolve();
        })
    });


    this.When(/^he selects the 160px option$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            selectThe160pxOption.of(),
        );
    });


    this.When(/^he clicks the edit widget button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            editSuperWidget.of(),
        );
    });

    this.When(/^he enters the label of the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterLabel(),
        );
    });

    this.When(/^he enters the description of the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterDescription(),
        );
    });

    this.When(/^he selects the sensor for the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.selectTheButtonSensor(),
        );
    });

    this.When(/^he ticks the override sensor properties for the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.clickOverrideSensorProperties(),
        );
    });

    this.When(/^he sets the min range of the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterMinValue(),
        );
    });

    this.When(/^he sets the max range of the widget$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterMaxValue(),
        );
    });

    this.When(/^he selects the unit from the dropdown$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.selectUnitFromDropdown(),
        );
    });


    this.When(/^he adds a range$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.clickAddRangeButton(),
        );
    });

    this.When(/^he enters the details for range 1$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterRange1Label(),
            // fillOutSensorDetails.enterToRange1(),
        );
    });

    this.When(/^he enters the details for range 2$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.enterRange2Label(),
            // fillOutSensorDetails.enterFromRange2(),
        );
    });

    this.When(/^he clicks the add to dashboard button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            fillOutSensorDetails.clickAddToDashboardButton(),
        );
    });
// TODO remove promise and replace with ActorInTheSpotlight
    this.When(/^he deletes the widget$/, async function () {
        return new Promise(async (resolve, reject) => {
            await deleteWidget();
            resolve();
        })
    });
};
