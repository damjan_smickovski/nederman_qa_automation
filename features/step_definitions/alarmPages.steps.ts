import { alarmActions } from './../../test/alarmPage/alarmPage';
import { helpers, getDataFromRuleFile, killProcess } from '../../scripts/helpers'
import { browser } from 'protractor';

export = function ruleEngineSteps() {
    this.Then(/^the alarm title should be "([^"]*)"$/, async function (alarmName: string) {
        let rule: any = await getDataFromRuleFile();
        rule = rule[rule.length - 1];
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmActions.checkAlarmName(rule)
        );
    });

    this.When(/^he clicks the acknowledge button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmActions.clickAcknowledgeButton()
        );
    });

    this.When(/^he clicks the handle button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmActions.clickHandleButton()
        );
    });

    this.When(/^he clicks the add corrective action button$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmActions.clickAddCorectionButton()
        );
    });

    this.Then(/^the timestamp should be saved and match$/, async function () {
        return this.stage.theActorInTheSpotlight().attemptsTo(
            alarmActions.checkIfAcknowledgeTimestampIsCorrect()
        );
    });


    this.When(/^he refreshes the page$/, async function () {
        return browser.refresh();
    });

    this.Then(/^the alarm should be auto closed$/, async function () {
        alarmActions.checkIfAlarmWasAutoClosed();
    });

};
