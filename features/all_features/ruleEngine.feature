Feature: Rule engine
      Creating rules and testing the alarm logic
      TODO change HE
      @rule
      Scenario: User creates rule and checks if alarm was raised
            Given Damjan navigates to the Nederman environment
            When he enters his email "nederman.automation@outlook.com"
            And he clicks the next button
            And he enters his password "damjan123"
            And he clicks the sign in button
            And he clicks no on the reduce login times
            Then he should be logged in
            When he selects Nederman Holding from the tenant dropdown
            When he clicks the administration button
            And he clicks the things button
            And he clicks the rule engine button
            And he clicks the add new button
            And he selects "Nederman Holding AB" from the company dropdown
            And he selects the "Skopje" plant
            And he selects the "On Demand Ventilation" system
            And he enters the rule name "01 RA"
            And he enters the rule desc "Automation description"
            And he selects the alarm type "C"
            And he clicks the add condition button
            And he sets the condition sensor to "light_right"
            And he sets the sensor operator to "<"
            And he sets the sensor value to "60"
            And he sets the in alarm time as "1"
            And he sets the out alarm time as "2"
            And he selects the "light_right" sensor from the sensor value dropdown
            And he sets the "medium" priority
            And he sets the subscriber as "Automation"
            And he enables the rule
            And he saves the rule
            Then the rule should be created
            And mock data succesfully generated as SERIES: "skopje", FROM: "2019/03/11 08:49:42.896 UTC", TO: "2019/03/11 08:56:12.366 UTC", NAME: "2MinIn2MinOut"
            And he runs the mocked data file "2MinIn2MinOut"
            And he he waits for "200" seconds
            When he navigates to the rule link from his email
            And he he waits for "10" seconds
            Then the alarm title should be ""


      @rule
      Scenario: User creates rule and acknowledges alarm
            Given Damjan navigates to the Nederman environment
            When he enters his email "nederman.automation@outlook.com"
            And he clicks the next button
            And he enters his password "damjan123"
            And he clicks the sign in button
            And he clicks no on the reduce login times
            Then he should be logged in
            When he selects Nederman Holding from the tenant dropdown
            When he clicks the administration button
            And he clicks the things button
            And he clicks the rule engine button
            And he clicks the add new button
            And he selects "Nederman Holding AB" from the company dropdown
            And he selects the "Skopje" plant
            And he selects the "On Demand Ventilation" system
            And he enters the rule name "01 RA"
            And he enters the rule desc "Automation description"
            And he selects the alarm type "C"
            And he clicks the add condition button
            And he sets the condition sensor to "light_left"
            And he sets the sensor operator to "<"
            And he sets the sensor value to "60"
            And he sets the in alarm time as "1"
            And he sets the out alarm time as "2"
            And he sets the auto close time as "10"
            And he selects the "light_left" sensor from the sensor value dropdown
            And he sets the "medium" priority
            And he sets the subscriber as "Automation"
            And he enables the rule
            And he saves the rule
            Then the rule should be created
            And mock data succesfully generated as SERIES: "skopje", FROM: "2019/03/28 09:44:34.775 UTC", TO: "2019/03/28 09:49:34.872 UTC", NAME: "autoCloseAfter3Mins"
            And he runs the mocked data file "autoCloseAfter3Mins"
            And he he waits for "200" seconds
            When he navigates to the rule link from his email
            And he he waits for "10" seconds
            Then the alarm title should be ""
            When he clicks the acknowledge button
            Then the timestamp should be saved and match




      @rule
      Scenario: User creates rule and that requires corrective action
            Given Damjan navigates to the Nederman environment
            When he enters his email "nederman.automation@outlook.com"
            And he clicks the next button
            And he enters his password "damjan123"
            And he clicks the sign in button
            And he clicks no on the reduce login times
            Then he should be logged in
            When he selects Nederman Holding from the tenant dropdown
            When he clicks the administration button
            And he clicks the things button
            And he clicks the rule engine button
            And he clicks the add new button
            And he selects "Nederman Holding AB" from the company dropdown
            And he selects the "Skopje" plant
            And he selects the "On Demand Ventilation" system
            And he enters the rule name "01 RA"
            And he enters the rule desc "Automation description"
            And he selects the alarm type "C"
            And he clicks the add condition button
            And he sets the condition sensor to "light_left"
            And he sets the sensor operator to "<"
            And he sets the sensor value to "60"
            And he sets the in alarm time as "1"
            And he sets the out alarm time as "1"
            And he clicks on the corrective action checkbox
            And he selects the "light_left" sensor from the sensor value dropdown
            And he sets the "medium" priority
            And he sets the subscriber as "Automation"
            And he enables the rule
            And he saves the rule
            Then the rule should be created
            And mock data succesfully generated as SERIES: "skopje", FROM: "2019/03/28 09:44:34.775 UTC", TO: "2019/03/28 09:49:34.872 UTC", NAME: "autoCloseAfter3Mins"
            And he runs the mocked data file "autoCloseAfter3Mins"
            And he he waits for "200" seconds
            When he navigates to the rule link from his email
            And he he waits for "10" seconds
            Then the alarm title should be ""
            #And he he waits for "60" seconds
            #And he refreshes the page
            When he clicks the handle button
            And he clicks the add corrective action button
            When he enters "01 CA" as the CA name
            And he selects "Compliance" as the CA type
            When selects the start date
            And selects the end date
            When he enters "Random cause" as the CA cause
            And he enters "Random response" as the CA response
            And he clicks the add CA button
            When he clicks the alarm center button
            When he selects the "Handled" alarm status from the dropdown
            Then the alarm status should be "Handled"



      @rule
      Scenario: User creates rule and checks if it was auto closed
            Given Damjan navigates to the Nederman environment
            When he enters his email "nederman.automation@outlook.com"
            And he clicks the next button
            And he enters his password "damjan123"
            And he clicks the sign in button
            And he clicks no on the reduce login times
            Then he should be logged in
            When he selects Nederman Holding from the tenant dropdown
            When he clicks the administration button
            And he clicks the things button
            And he clicks the rule engine button
            And he clicks the add new button
            And he selects "Nederman Holding AB" from the company dropdown
            And he selects the "Skopje" plant
            And he selects the "On Demand Ventilation" system
            And he enters the rule name "01 RA"
            And he enters the rule desc "Automation description"
            And he selects the alarm type "C"
            And he clicks the add condition button
            And he sets the condition sensor to "light_left"
            And he sets the sensor operator to "<"
            And he sets the sensor value to "60"
            And he sets the in alarm time as "1"
            And he sets the out alarm time as "1"
            And he sets the auto close time as "1"
            And he selects the "light_left" sensor from the sensor value dropdown
            And he sets the "medium" priority
            And he sets the subscriber as "Automation"
            And he enables the rule
            And he saves the rule
            Then the rule should be created
            And mock data succesfully generated as SERIES: "skopje", FROM: "2019/03/28 09:44:34.775 UTC", TO: "2019/03/28 09:49:34.872 UTC", NAME: "autoCloseAfter3Mins"
            And he runs the mocked data file "autoCloseAfter3Mins"
            And he he waits for "230" seconds
            When he navigates to the rule link from his email
            And he he waits for "10" seconds
            Then the alarm title should be ""
            When he refreshes the page
            And he he waits for "10" seconds
            Then the alarm should be auto closed
