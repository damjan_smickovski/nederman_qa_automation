const isCI = () => process.env.CI && process.env.CI === 'true';

/**
 * To learn more about the protractor.conf.js check out:
 * https://github.com/angular/protractor/blob/master/lib/config.ts
 */
exports.config = {

    onPrepare: function () {
        browser.ignoreSynchronization = true;
        restartBrowserBetweenTests = true;
        var width = 1920;
        var height = 1080;
        browser.driver.manage().window().setSize(width, height);
    },

    directConnect: true,

    // https://github.com/angular/protractor/blob/master/docs/timeouts.md
    allScriptsTimeout: 500000,
    serenity: {
        stageCueTimeout: 100 * 400   // up to 30 seconds by default
    },
    // Load Serenity/JS
    framework: 'custom',
    frameworkPath: require.resolve('serenity-js'),

    specs: ['features/**/*.feature'],

    cucumberOpts: {
        require: ['features/**/*.ts'],
        format: 'pretty',
        compiler: 'ts:ts-node/register',
        tags: ['@rule'],
        format: 'json:cucumber_report.json', 
    },

    capabilities: {
        browserName: 'chrome',

        chromeOptions: {
            args: [
                '--disable-infobars',
                '--headless',
                '--no-sandbox',
                '--disable-gpu',

            ].

                // Required on Travis CI when running the build without sudo
                // https://developers.google.com/web/updates/2017/06/headless-karma-mocha-chai#running_it_all_on_travis_ci
                concat(isCI() ? ['--no-sandbox'] : [])
        }
    }
};
