import { Wait, Duration, Task } from 'serenity-js/lib/screenplay-protractor';
import request from "node-fetch";
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';
import * as fs from 'fs';
const ruleIdpath = `${__dirname}/../tmp/createdRuleList.json`;
const util = require('util');
const exec = util.promisify(require('child_process').exec);
var spawn = require('child_process').spawn;

// Returns the data from createdRuleList.json if it doesn't exists it creates it
export function getDataFromRuleFile() {

    let exists = fs.existsSync(ruleIdpath)

    if (!exists) {
        fs.writeFileSync(ruleIdpath, '');
        logger.debug(`Created ${ruleIdpath}`);
    }
    else {
        logger.debug(`File exists ${ruleIdpath}, continuing`);
    }

    const currentData: string[] = JSON.parse(
        fs.readFileSync(ruleIdpath).toString() || "[]"
    );
    //logger.debug(`Returning data as: ${JSON.stringify(currentData)}`)
    return currentData;
}
// kills the running process from qa.pid
export function killProcess() {
    fs.exists(`${__dirname}/../qa.pid`, (exists) => {
        if (exists) {
            fs.readFile(`${__dirname}/../qa.pid`, 'utf8', function (err, PID) {
                if (err) {
                    logger.console.error("Couldn't read process file");
                    return;
                }
                let process = spawn("kill", [PID]);

                process.stdout.on('data', function (data) {
                    logger.debug('stdout: ' + data.toString());
                    logger.debug(`Successfully killed: ${PID}`)
                });

                process.stderr.on('data', function (data) {
                    logger.debug('stdout: ' + data.toString());
                });

            });
        }
        else
            logger.console.error('Couldn\'t kill process, no PID file found');
    })
}

export const helpers = {

    //Adds an entry for the newly created rule to ../tmp/createdRuleList.txt
    addEntryToRuleData: (ruleToAdd: string) => {
        let currentData: string[] = getDataFromRuleFile()
        currentData.push(ruleToAdd);
        fs.writeFileSync(ruleIdpath, JSON.stringify(currentData));
        logger.debug(`Current object entries: ${JSON.stringify(currentData)}`);
        currentData = [];
    },

    //deletes all rules from DB that are in the ../tmp/createdRuleList.txt
    deleteRuleEntries: async () => {
        logger.debug('Preparing to delete existing rules...');

        const currentData = getDataFromRuleFile();

        const requestArray = currentData.map(entry => {
            return request(
                "https://n7utie1pze.execute-api.eu-west-1.amazonaws.com/test/rules", {
                    method: "POST",
                    body: JSON.stringify({
                        name: entry
                    }),
                    headers: {
                        'Content-Type': `application/json; domain-model=delete`
                    }
                }).then(res => { return res.json() })
        });

        const responses = await Promise.all(requestArray);
        for (let i = 0; i < responses.length; i++) {
            if (responses[i].code === 404) {
                logger.debug(`Couldnt delete rule: \n${responses[i].message}`);
                logger.debug(responses[i]);
                let ruleName = responses[i].message.substring(responses[i].message.indexOf("name:") + 5, responses[i].message.indexOf("not found") - 1);
                //console.log(ruleName);
                const currentData = getDataFromRuleFile();
                const dataForUpdate = currentData.filter(entry => !ruleName.includes(entry));
                fs.writeFileSync(ruleIdpath, JSON.stringify(dataForUpdate));
            }
            else {

                const currentData = getDataFromRuleFile();
                const dataForUpdate = currentData.filter(entry => !responses[i].name.includes(entry));
                fs.writeFileSync(ruleIdpath, JSON.stringify(dataForUpdate));
                logger.debug(`Deleted rule "${responses[i].name}"`);
            };
        }
    },

    //creates dump data based on provided timestamp
    createDumpData: async (series: string, beginTimestamp: Date, endTimestamp: Date, outputFileName: string) => {
        const { stdout, stderr } = await exec(`exeriot dump -s ${series} -t "${beginTimestamp}" -u "${endTimestamp}" -f "${__dirname}/${outputFileName}" -c ${__dirname}/exeriot.json`);
        //logger.debug('stdout:', stdout);
        //logger.debug('stderr:', stderr);
        logger.debug(`Dumped parquet file as ${outputFileName}`);
    },

    //runs the dumped data async
    runMockData: async (outputFileName: string) => {
        //const { stdout, stderr } = exec(`exeriot run -c ${__dirname}/exeriot.json -f ${__dirname}/${outputFileName}.parquet`);
        let exeriot = spawn('exeriot', ['run', '-c', `${__dirname}/exeriot.json`, '-f', `${__dirname}/${outputFileName}.parquet`]);
        fs.writeFile(`${__dirname}/../tmp/exeriotRunOutput-${outputFileName}.txt` || `${__dirname}/../tmp/exeriotRunOutput-${outputFileName}.txt`, '', err => {
            if (err)
                logger.debug(err);
        })
        exeriot.stdout.on('data', function (data) {
            logger.error('stdout: ' + data.toString());
        });
        // Dumps the data in a txt file
        exeriot.stderr.on('data', function (data) {
            // console.log('stderr: ' + data.toString());
            fs.appendFile(`${__dirname}/../tmp/exeriotRunOutput-${outputFileName}.txt` || `${__dirname}/../tmp/exeriotRunOutput-${outputFileName}.txt`, data.toString(), (err) => {
                if (err)
                    logger.debug(`Something went wrong + ${err}`);
            })
        });

        exeriot.on('exit', function (code) {

            logger.debug('child process exited with code ' + code);
        });

    },

    // Hard wait function
    hardWait: (timeTowait: number) => {
        logger.debug(`Initiating hard wait for ${timeTowait}s`);
        return Task.where(`#actor hard waits`,
            Wait.for(Duration.ofSeconds(timeTowait))
        )
    },

    // Returns the rule id based on name input
    getRuleIdByName: async (alarmName: string) => {
        const response = await request('https://n7utie1pze.execute-api.eu-west-1.amazonaws.com/test/rules', {
            method: "POST",
            headers: {
                'Content-Type': `application/json; domain-model=getRuleByName`
            },
            body: JSON.stringify({
                name: alarmName
            })
        });
        return response.json();
    },

    getLatestEntryFromRuleIdFile: async () => {
        let rule: any = getDataFromRuleFile();
        rule = rule[rule.length - 1];
        return rule;
    }

};
helpers.deleteRuleEntries();


