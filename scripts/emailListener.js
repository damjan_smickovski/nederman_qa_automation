
const inbox = require('inbox');
var log4js = require('log4js');
var logger = log4js.getLogger();
logger.level = 'debug';
let ruleName;
const Stream = require('stream')
const readableStream = new Stream.Readable()
var link = '';

var client = inbox.createConnection(993, "outlook.office365.com", {
  secureConnection: true,
  auth: {
    user: "nederman.automation@outlook.com",
    pass: "damjan123"
  }
});


// Gets the email message based on the UID provided and returns the link to the alarm
async function fetchMessage(uid) {
  let result = '';
  let counter = 0;
  return new Promise((resolve, reject) => {
    try {
      let readableStream = client.createMessageStream(uid);
      readableStream.on('data', chunk => {
        result += chunk.toString('UTF-8');
      }).on('end', function () {
        let indexOfLink = result.indexOf("https://test.iot");
        result = result.substring(indexOfLink, indexOfLink + 150);
        indexOfLink = result.indexOf("\"");
        result = result.substring(0, indexOfLink);
        result = result.replace(/=|\n|\r/g, '')
        resolve(result);
      })
    }
    catch (e) {
      reject(e);
    }
  });
}

// Connects to the email inbox, finds a message matching ${rulename} and returns a link to the rule
async function connectToclient() {
  client.connect();
  let counter = 0;
  return new Promise(async (resolve, reject) => {
    client.on("connect", async function () {
      client.openMailbox("INBOX", (error, info) => {
        if (error) reject(error);
        logger.debug("Message count in INBOX: " + info.count);
        client.listMessages(-10, function (err, messages) {
          let messagesInbox = messages.map((message) => {
            if (message.title.includes(ruleName)) {
              link = fetchMessage(message.UID);
            }
          });
          client.close();
          resolve(link);
        });
      });
    });
  });

}

client.on('close', async function () {
  logger.debug("DISCONNECTED");
});

module.exports = {
  fetchMessages: async (alarmName) => {
    return new Promise(async (resolve, reject) => {
      logger.debug(`Searching for: ${alarmName}`);
      ruleName = alarmName;
      let link = await connectToclient().then(result => { if (result != "" && result != undefined) { logger.debug(`Fetched link: ${result}`); resolve(result); } else reject(`Couldn't find message "${alarmName}" in INBOX`) });
    });
  },

  closeClient: async () => {
    client.close();
    Promise.resolve();
  }
}

// async function test1() {
//   test = await module.exports.fetchMessages('01 RA - 96e26e61-191a-4ba6-8085-4e6da3f4cdad').catch(e => { Promise.reject(e) })
//   console.log(test)
// }
// test1()