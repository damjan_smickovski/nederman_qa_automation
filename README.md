# Frontend Automation framework

### **Pre-requisites**
- JRE (any)
- NodeJS >= v10.15.2 ([Windows Download link](https://nodejs.org/en/download/), [UNIX based OS instructions](https://nodesource.com/blog/installing-node-js-tutorial-using-nvm-on-mac-os-x-and-ubuntu/))
- NPM >= v6.4.1 (installed alongside with NodeJS)
- Protractor >= v5.4.2 (run `npm install -g protractor` - requires admin or sudo priviliges)

### **Structure and hierarchy**
1. **Feature files `features/*.feature`** - Top level files that define the scenario that will be executed using the Gherkin format (Given, When, Then)
2. **Step definition files `features/step_definitions/*.steps.ts`** - Layer 2 files that contain the mappings for each step mentioned from the feature file
3. **Page object files `pageObjects/**/{nameOfPage}.ts}`** - Layer 3 files that contain the actions and assertions for elements on each given page
4. **Element locator files `pageObjects/**/ui/{nameOfPage}Elements.ts`** - Layer 4 files that serve as a centralized location of element locators for each page
5. **Target folder** - Create after executing a scenario(s) which generates an html report that can be found at `target/site/serenity/index.html`
6. **Protractor configuration file `protractor.conf.js`** - Contains the configurations of the execution environment
7. **Email listener `emailListener.js`** - Connects to the outlook smtp server and returns the matching rule email
8. **Helper functions `helpers.ts`** - Contains various functions that create and delete temporary files
9. **Created rules object `createdRuleList.json`** - During every run the created rule is stored here and when `npm test` if run the stored rules are deleted from the DB
10. **Exeriot run output `exeriotRunOutput{nameOfFileBeingRun}`** - Contains the piped otput from the `exeriot run` command
11. **Cucumber JSON report `cucumber_report.json`** - Contains results in JSON for the Slack notification webhook


### How to run any scenario(s)
1. Clone this repo
2. run `npm install` in this folder
3. run `npm test`

### Notes
- The headless option can be removed in the `protractor.conf.js` file
- A defined and working step in step definitions can be used infinite times inside a feature file

### Sample report
- [Jenkins Report](http://ec2-18-202-197-85.eu-west-1.compute.amazonaws.com:8080/job/FE_automation/HTML_20Report/)
- Jenkins Credentials
  - Username: admin
  - Password: Ask Damjan

### Documentation
- [Nodejs](https://nodejs.org/en/docs/)
- [NPM](https://docs.npmjs.com/)
- [protractor](https://www.protractortest.org/#/api)
- [serenityJS](https://serenity-js.org/)
- [cucumberJS](https://github.com/cucumber/cucumber-js)
